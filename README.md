find_blood_donors_1

Procedure: Search for profiles with blood type "AB-", aged 60 and named Victoria, only those with an email address @gmail.com.


find_blood_donors_2(filename)

Procedure: Search for male profiles, aged 30 to 50 years old with blood type "B+", surname Norris and who have an email address @yahoo.com.
