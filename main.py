from datetime import datetime, date
from csv import DictReader

def calculate_age(birthdate):
    obj_birthdate = datetime.strptime(birthdate, '%Y-%m-%d').date()
    today = date.today()
    return today.year - obj_birthdate.year - ((today.month, today.day) < (obj_birthdate.month, obj_birthdate.day))


def find_select_blood_donors_1(filename):
    target_age = 60

    with open(filename, 'r') as file:
        csv_reader = DictReader(file)

        for profile in csv_reader:
            age = calculate_age(profile['birthdate'])
            if age == target_age and profile['blood_group'] == 'AB-'\
                and profile['name'].startswith('Victoria')\
                    and profile['mail'].endswith('@yahoo.com'):
                yield {
                    profile['name'],
                    profile['mail']
                }

def find_blood_donors_2(filename):
    minimum_age = 30
    maximum_age = 50

    with open(filename, 'r') as file:
        csv_reader = DictReader(file)

        for each_line_profile in csv_reader:
            age = calculate_age(each_line_profile['birthdate'])
            if age >= minimum_age and age <= maximum_age \
                and each_line_profile['blood_group'] == 'B+' \
                and each_line_profile['name'].endswith('Norris') \
                and each_line_profile['mail'].endswith('@yahoo.com') \
                and each_line_profile['sex'] == 'M':
                    yield {
                        each_line_profile['name'],
                        each_line_profile['mail']
                    }